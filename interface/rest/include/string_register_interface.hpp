#pragma once

#include <restbed>
#include <caf/all.hpp>

namespace interface {
  using namespace std;
  using namespace restbed;
  using namespace caf;

  void storage_sr_get_handler(shared_ptr<Session> const session, actor const& key_manager);
	void storage_sr_put_handler(shared_ptr<Session> const session, actor const& key_manager);
	void storage_sr_post_handler(shared_ptr<Session> const session, actor const& key_manager);
	void storage_sr_delete_handler(shared_ptr<Session> const session, actor const& key_manager);
}
