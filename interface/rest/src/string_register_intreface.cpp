#include <string_register_interface.hpp>
#include <caf/all.hpp>

#include <CRDTCell.hpp>
#include <StringRegisterActor.hpp>

namespace interface {

  using namespace caf;
  using namespace std;
  using namespace restbed;

  void storage_sr_get_handler(shared_ptr<Session> const session, actor const& key_manager) {
		const auto request = session->get_request();
		const string name = request->get_path_parameter("name");
		//const string sitem = request->get_path_parameter("item");

		//parse item
		//const auto item = to_data_type(sitem);
		
		//find aworset
		auto sreg_ptr = key_manager.home_system().registry().get(name);

		if (!sreg_ptr) {
			session->close(NOT_FOUND, "String register with name \"" + name + "\" not found");
			return;
		}

		scoped_actor self{ key_manager.home_system() };

		self->request(actor_cast<actor> (sreg_ptr), 15s, core::get_all_data{}, self).receive(
			[=](std::string  result) {
				session->close(OK, result + "\n");
			},
			[=, &self](error err)mutable  {
        aout(self) <<"Get Error:" <<err<<endl;
				session->close(INTERNAL_SERVER_ERROR, "Error");
			}
		);		
	}

	//creates new aworset with name
	void storage_sr_put_handler(shared_ptr<Session> const session, actor const& key_manager) {
		const auto request = session->get_request();
		const string name = request->get_path_parameter("name");

		anon_send(key_manager, core::add_elem{}, core::string_reg_atom{}, name, std::chrono::milliseconds{100});
		
		session->close(OK, "String Register Processed\n");
	}

	//inserts new element into set
	void storage_sr_post_handler(shared_ptr<Session> const session, actor const& key_manager) {
		const auto request = session->get_request();
		const string name = request->get_path_parameter("name");
		const string sitem = request->get_path_parameter("item");
    
		//find aworset
		auto sreg_ptr = key_manager.home_system().registry().get(name);

		if (!sreg_ptr) {
			session->close(NOT_FOUND, "String register with name \"" + name + "\" not found");
			return;
		}

    cout <<"Put "<<sitem<<endl;
		anon_send(actor_cast<actor> (sreg_ptr), core::add_elem{}, sitem);
		
		session->close(OK, "String Register POST OK\n");
	}

	//deletes aworset
	void storage_sr_delete_handler(shared_ptr<Session> const session, actor const& key_manager) {
		const auto request = session->get_request();
		const string name = request->get_path_parameter("name");

		anon_send(key_manager, core::rmv_elem{}, name);

		session->close(OK, "Processed\n");
	}

};
