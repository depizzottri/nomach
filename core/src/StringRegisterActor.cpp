#include <caf/all.hpp>

#include <StringRegister.hpp>
#include <StringRegisterActor.hpp>
#include <chrono>
#include <CRDTCell.hpp>
#include <fun_types.hpp>

namespace core {
    using namespace caf;
    using namespace std;

    behavior string_register_actor(stateful_actor<StringRegister_actor_state>* self, string name, string node_name) {
        self->state.reg = SLWWReg{};//SLWWReg(node_name);

        self->system().registry().put(name, actor_cast<strong_actor_ptr> (self));

        self->attach_functor(
            [=](const error& reason) {
                self->system().registry().erase(name);
                aout(self) <<"Reg actor exited " << reason<<endl;
            }
        );

        aout(self) <<"Reg actor spawned " << name<<endl;

        return {
            [=](add_elem, std::string const& data) {
              self->state.reg.write(
                    std::chrono::steady_clock::now().time_since_epoch().count(), 
                    data
                    );
              
              for(auto& a: self->state.subs) {
                self->send(a, add_elem::value, data);
              }
            },
            [=](merge_data, SLWWReg const& other) { //5.6%
                self->state.reg.join(other);
            },
            [=](get_all_data, actor sender) {
                return make_message(self->state.reg.read());
            },
            [=](get_raw_data) {
                return make_message(self->state.reg);
            },
            [=](crdt_name) {
                return make_message(name);
            },
            [=](subs_atom, actor who) {
              self->state.subs.push_back(who);
              self->send(who, add_elem::value, self->state.reg.read());
            }
            
        };
    }

    behavior string_register_map_actor(
        stateful_actor<StringRegister_actor_state>* self, 
        string name,
        actor reg1,
        FunTypes fun_type,
        string node_name
        ) {
        self->state.reg = SLWWReg{};

        self->system().registry().put(name, actor_cast<strong_actor_ptr> (self));

        self->attach_functor(
            [=](const error& reason) {
                self->system().registry().erase(name);
                aout(self) <<"Reg actor exited" << reason<<endl;
            }
        );

        aout(self) <<"Reg MAP actor spawned " << name<<endl;

        return {
            [=](add_elem, std::string const& data) {
               aout(self) <<"MAP Reg: " <<data<<endl;
               string res_data;
               switch(fun_type) {
                 case(fun_type_double): {
                   res_data = data+data;
                   break;
                                        }
                 case(fun_type_to_upper): {
                  for(auto c: data) {
                    res_data += std::toupper(c);
                  }
                  break;
                                          }
               }
               self->state.reg.write(
                 std::chrono::steady_clock::now().time_since_epoch().count(),
                 res_data
               );
               for(auto& a: self->state.subs) {
                 self->send(a, add_elem::value, data);
               }
            },
            [=](merge_data, core::SLWWReg const& other) { //5.6%
                self->state.reg.join(other);
            },
            [=](get_all_data, actor sender) {
               return make_message(self->state.reg.read());
            },        
            [=](get_raw_data) {
                return make_message(self->state.reg);
            },
            [=](crdt_name) {
                return make_message(name);
            },
            [=](subs_atom, actor who) {
              self->state.subs.push_back(who);
              self->send(who, add_elem::value, self->state.reg.read());
            }
        };     

    }
} //namespace

