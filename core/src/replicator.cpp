#include <caf/all.hpp>

#include <AWORSet.hpp>
#include <StringRegister.hpp>
#include <AWORSetActor.hpp>
#include <StringRegisterActor.hpp>
#include <clustering.hpp>
#include <utils.hpp>
#include <replicator.hpp>
#include <remoting.hpp>
#include <fun_types.hpp>

using namespace caf;
using namespace std;

namespace core {

    replicator_config::replicator_config() {
        add_message_type<AWORSet>("intAWORSet");
        add_message_type<SLWWReg>("StringReg");
    }

    std::string replicator_name(remoting::actor_name const& name) {
        return name + "repl";
    }

    //TODO: why sync erlier then "member up"
    //TODO: fix non-syncing

    //TODO: remove cluster_member dependecy
    actor spawn_intaworset(actor_system& system, string const& name, actor cluster_member, string const& node_name, ::remoting::remoting rem, chrono::milliseconds tick_interval) {
        auto set_actor = system.spawn(AWORSet_actor, name, node_name);
        auto repl_actor = system.spawn(replicator_actor<AWORSet>, name, set_actor, cluster_member, rem, tick_interval);

        return set_actor;
    }

    actor spawn_string_register(actor_system& system, string const& name, actor cluster_member, string const& node_name, ::remoting::remoting rem, chrono::milliseconds tick_interval) {
        auto reg_actor = system.spawn(string_register_actor, name, node_name);
        auto repl_actor = system.spawn(replicator_actor<SLWWReg>, name, reg_actor, cluster_member, rem, tick_interval);
        return reg_actor;
    }

    actor spawn_reg_map_actor(
        actor_system& system,
        string const& new_name,
        actor reg1,
        FunTypes fun_type,
        actor cluster_member,
        string const& node_name,
        ::remoting::remoting rem,
        chrono::milliseconds tick_interval
    ) {
          auto reg_actor = system.spawn(string_register_map_actor, new_name, reg1, fun_type, node_name);
          auto repl_actor = system.spawn(replicator_actor<SLWWReg>, new_name, reg_actor, cluster_member, rem, tick_interval);
          return reg_actor;
   }
}
