#pragma once

#include <caf/all.hpp>

#include "AWORSet.hpp"
#include <fun_types.hpp>

namespace core {

    using namespace caf;
    using namespace std;

    using crdt_name = atom_constant<atom("crdtname")>;

    //using set_type = set<data_type>;
    using SLWWReg = lwwreg<uint64_t, std::string>;

    //using AWORSetActor = typed_actor <
    //    reacts_to<add_elem, data_type>,
    //    reacts_to<rmv_elem, data_type>,
    //    replies_to<get_all>::with<set_type>,
    //    replies_to<get_raw_data>::with<AWORSet>
    //>;
    //
    
    using subs_atom = atom_constant<atom("subss")>;

    struct StringRegister_actor_state {
         SLWWReg reg;
         vector<actor> subs;
    };

    behavior string_register_actor(stateful_actor<StringRegister_actor_state>* self, string name, string node_name);

    behavior string_register_map_actor(
        stateful_actor<StringRegister_actor_state>* self, 
        string name, 
        actor reg1,
        FunTypes fun_type,
        string node_name
    );
}
