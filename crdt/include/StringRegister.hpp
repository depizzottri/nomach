#pragma once

//from https://github.com/CBaquero/delta-enabled-crdts
#include <delta-crdts.cc>
#include <caf/all.hpp>

template <class Inspector>
auto inspect(Inspector& f, lwwreg<uint64_t, std::string>& x) { //9%
    return f(caf::meta::type_name("LWWREG"), x.r);
}
