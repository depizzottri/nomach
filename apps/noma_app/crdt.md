```
CRDT (Conflict-free Replicated DataType) = 
{
  data: Partial Ordered Set
  update() - monotonic op on data
  merge() - Lattice Inf (Sup)
  read() - read query op
}

GCounter = 
{
  //i - replica Id
  data: Version Vector;
  update() =  data[i] = data[i] + 1;
  merge(other) = for all i: data[i] = max(data[i], other.data[i]);
  read() = sum(data[i]) for all i;
}

GSet =
{
  data: <A:Set, R:Set>;
  add(a) = A unite {a};
  rmv(a) = R unite {r};
  merge(other) = A = A unite other.A; B = B unite other.B;  
  read() = A diff B
}

MVRegister<DataType> = 
{
  data: <d:Array<DataType>, v:VersionVector>;
  update(newData) = d = [newData]; v.event();
  merge(other) = if(other.v || v) then d.add(other.d)
                 else if(other.v < v) then d = other.d;
                 v = merge(v, other.v)
  read() = d
}
```
