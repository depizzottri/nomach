Eventual Consistency

CRDT - Conflict-free Replicated DataType

[GCounter, GSet, MVRegister](crdt.md), PNCounter,  ORSet, AWORSet, (X)Map, RGA, JSON

CAP

eCAP

Lattice Programming 

Functional Reactive Programming

==============================

```
Register<string> a = "aaaa"
Register<string> b = "bbb"

Register<string> c = map(a, x => a + c)

RGA<int> f;
update(f, 0, 1)
update(f, 1, 2)
update(f, 2, 3)
//f == [1,2,3]

RGA<int> f1 = map(f, x => x+1)
//f1 == [2,3,4]
RGA<int> f2 = filter(f1, x => x % 2 == 0)
//f2 == [2,4]
Register<int> f3 = fold(f2, 0, (x,y) => x+y)
//f3 == 6

update(f, 0, 6)
//f1 == [7, 2, 3]
//f2 == [2]
//f3 == 2
```

===============================
```
Register<string> one = "asd"
Register<string> two = "qwe"

Register<string> m_one = map(one, x => x+x)
//m_one == "asdasd"
Register<string> m_two = map(two, x => to_upper(x))
//m_two == "QWE"

one = afhwe
//m_one == afhweafhwe
two = AsRdTv
//m_two == ASRDTV
```

Demonstration:

```./noma_app --name=seed1 --port=6666 --http_port=14780```

```./noma_app --name=node1 --port=6668 --http_port=14781```

```curl -XPUT http://localhost:14780/s_register/one```

```curl -XPOST http://localhost:14780/s_register/one/asd```

```curl -XGET http://localhost:14780/s_register/one```

```curl -XPUT http://localhost:14781/s_register/two```

```curl -XPOST http://localhost:14781/s_register/two/qwe```

```curl -XGET http://localhost:14781/s_register/two```

```curl -XPOST 'http://localhost:14780/map/double?reg=one'```

```curl -XPOST 'http://localhost:14781/map/toupper?reg=two'```

```curl -XGET http://localhost:14781/s_register/m_one```

```curl -XGET http://localhost:14780/s_register/m_two```

```curl -XPOST http://localhost:14780/s_register/one/afhwe```

```curl -XGET http://localhost:14781/s_register/m_one```

```curl -XPOST http://localhost:14780/s_register/two/AsRdTv```

```curl -XGET http://localhost:14781/s_register/m_two```
